package ru.smochalkin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.system.AccessDeniedException;

@Component
public class UserLogoutListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "logout";
    }

    @Override
    @NotNull
    public String description() {
        return "Log out.";
    }

    @Override
    @EventListener(condition = "@userLogoutListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        @NotNull final Result result =  sessionEndpoint.closeSession(sessionService.getSession());
        printResult(result);
        sessionService.setSession(null);
    }

}
