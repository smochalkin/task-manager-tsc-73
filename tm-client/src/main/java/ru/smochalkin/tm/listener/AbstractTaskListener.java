package ru.smochalkin.tm.listener;

import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.endpoint.TaskDto;
import ru.smochalkin.tm.exception.entity.TaskNotFoundException;

public abstract class AbstractTaskListener extends AbstractListener {

    protected void showTask(@Nullable final TaskDto task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus());
        System.out.println("Project: " + task.getProjectId());
    }

}
