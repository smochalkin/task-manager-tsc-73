package ru.smochalkin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractProjectListener;
import ru.smochalkin.tm.endpoint.ProjectDto;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectShowListListener extends AbstractProjectListener {

    @Override
    @NotNull
    public String name() {
        return "project-list";
    }

    @Override
    @NotNull
    public String description() {
        return "Display project list.";
    }

    @Override
    @EventListener(condition = "@projectShowListListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.println("Enter sort option from list:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final List<ProjectDto> projects;
        String sortName = TerminalUtil.nextLine();
        if (sortName.isEmpty()) {
            projects = projectEndpoint.findProjectAll(sessionService.getSession());
        } else {
            projects = projectEndpoint.findProjectAllSorted(sessionService.getSession(), sortName);
        }
        int index = 1;
        for (ProjectDto project : projects) {
            System.out.println(index++ + ". " + project.getId() + "|" + project.getName() + "|" + project.getStatus());
        }
    }

}
