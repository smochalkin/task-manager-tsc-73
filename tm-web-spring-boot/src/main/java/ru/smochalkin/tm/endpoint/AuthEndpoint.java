package ru.smochalkin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.model.CustomUser;

import javax.annotation.Resource;

@RestController
@RequestMapping("/auth")
public class AuthEndpoint {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

    @GetMapping("/login")
    public Boolean login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    ) {
        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(username, password);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return authentication.isAuthenticated();
    }

    @GetMapping("/session")
    public Authentication session() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @GetMapping("/profile")
    public CustomUser user(@AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user) {
        return user;
    }

    @GetMapping("/logout")
    public Boolean logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return true;
    }

}
