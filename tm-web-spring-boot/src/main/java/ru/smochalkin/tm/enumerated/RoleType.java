package ru.smochalkin.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum RoleType {

    ADMIN("administrator"),
    USER("user");

    @NotNull
    private final String displayName;

    RoleType(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
