<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Status</th>
        <th>Project</th>
        <th>Created</th>
        <th>Finish date</th>
        <th>Description</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.status.getDisplayName()}"/>
            </td>
            <td>
                <c:forEach var="project" items="${projects}">
                    <c:if test="${project.id == task.projectId}">
                        <c:out value="${project.name}"/>
                    </c:if>
                </c:forEach>
            </td>
            <td>
                <fmt:formatDate value="${task.created}" pattern="dd.MM.yyyy"/>
            </td>
            <td>
                <fmt:formatDate value="${task.endDate}" pattern="dd.MM.yyyy"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td>
                <a href="/task/edit/${task.id}">Edit</a>
            </td>
            <td>
                <a href="/task/delete/${task.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="/task/create">
    <button type="submit">Create</button>
</form>
<jsp:include page="../include/_footer.jsp"/>