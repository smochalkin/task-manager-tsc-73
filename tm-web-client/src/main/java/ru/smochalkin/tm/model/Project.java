package ru.smochalkin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_project")
public class Project extends AbstractBusinessEntity {

    public Project(final String name) {
        this.name = name;
    }

}