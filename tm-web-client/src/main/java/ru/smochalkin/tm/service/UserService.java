package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import ru.smochalkin.tm.api.service.IRoleService;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.enumerated.RoleType;
import ru.smochalkin.tm.exception.EmptyEmailException;
import ru.smochalkin.tm.exception.EmptyIdException;
import ru.smochalkin.tm.exception.EmptyLoginException;
import ru.smochalkin.tm.exception.UserNotFoundException;
import ru.smochalkin.tm.model.Role;
import ru.smochalkin.tm.model.User;
import ru.smochalkin.tm.repository.IUserRepository;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@EnableTransactionManagement(proxyTargetClass = true)
public class UserService implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @Autowired
    private IRoleService roleService;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMIN);
        initUser("user", "user", RoleType.USER);
    }

    private void initUser(
            @Nullable final String login, @Nullable final String password, @Nullable final RoleType roleType
    ) {
        @Nullable final User user = repository.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    public void createUser(
            @Nullable final String login, @Nullable final String password, @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        @NotNull final String passwordHah = passwordEncoder.encode(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHah);
        repository.save(user);
        @NotNull final Role role = new Role();
        role.setUserId(user.getId());
        if (roleType != null) role.setRole(roleType);
        roleService.add(role);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<User> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (User item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User add(@Nullable final User entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new)).orElse(null);
    }

    @Override
    @SneakyThrows
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @Nullable final User user = repository
                .findById(optionalId.orElseThrow(EmptyIdException::new)).orElse(null);
        if (user == null) return;
        repository.delete(user);
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final User entity) {
        if (entity == null) return;
        @Nullable final User user = repository.findById(entity.getId()).orElse(null);
        if (user == null) return;
        repository.delete(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return repository.findByLogin(login);
    }

    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return repository.findByEmail(email);
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = repository.findByLogin(login);
        if (user == null) return;
        repository.delete(user);
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }


    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLock(true);
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLock(false);
        repository.save(user);
        return user;
    }

}