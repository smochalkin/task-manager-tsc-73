package ru.smochalkin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.smochalkin.tm.dto.SessionDto;

import java.util.List;

public interface SessionRepository extends JpaRepository<SessionDto, String> {

    @NotNull
    List<SessionDto> findAllByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull String userId);

}
