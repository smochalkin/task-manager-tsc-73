package ru.smochalkin.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.api.endpoint.IUserEndpoint;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.dto.SessionDto;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.dto.result.Fail;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.result.Success;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @WebMethod
    @SneakyThrows
    public UserDto findUserBySession(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto
    ) {
        sessionService.validate(sessionDto);
        return userService.findById(sessionDto.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result userSetPassword(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "password") @Nullable final String password
    ) {
        sessionService.validate(sessionDto);
        try {
            userService.setPassword(sessionDto.getUserId(), password);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result userUpdate(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName") @Nullable final String middleName
    ) {
        sessionService.validate(sessionDto);
        try {
            userService.updateById(sessionDto.getUserId(), firstName, lastName, middleName);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
