package ru.smochalkin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.api.endpoint.IAdminEndpoint;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.dto.result.Fail;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.result.Success;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.dto.SessionDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @WebMethod
    public Result createUser(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) {
        sessionService.validate(sessionDto, Role.ADMIN);
        try {
            userService.create(login, password, email);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public Result removeUserById(@WebParam(name = "session") @NotNull final SessionDto sessionDto,
                                 @WebParam(name = "userId") @Nullable final String userId
    ) {
        sessionService.validate(sessionDto, Role.ADMIN);
        try {
            userService.removeById(userId);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public Result removeUserByLogin(@WebParam(name = "session") @NotNull final SessionDto sessionDto,
                                    @WebParam(name = "login") @Nullable final String login
    ) {
        sessionService.validate(sessionDto, Role.ADMIN);
        try {
            userService.removeByLogin(login);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public Result lockUserByLogin(@WebParam(name = "session") @NotNull final SessionDto sessionDto,
                                  @WebParam(name = "login") @Nullable final String login
    ) {
        sessionService.validate(sessionDto, Role.ADMIN);
        try {
            userService.lockUserByLogin(login);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public Result unlockUserByLogin(@WebParam(name = "session") @NotNull final SessionDto sessionDto,
                                    @WebParam(name = "login") @Nullable final String login
    ) {
        sessionService.validate(sessionDto, Role.ADMIN);
        try {
            userService.unlockUserByLogin(login);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public List<UserDto> findAllUsers(@WebParam(name = "session") @NotNull final SessionDto sessionDto) {
        sessionService.validate(sessionDto, Role.ADMIN);
        return userService.findAll();
    }

    @Override
    @WebMethod
    public List<SessionDto> findAllSessionsByUserId(@WebParam(name = "session") @NotNull final SessionDto sessionDto,
                                                    @WebParam(name = "userId") @Nullable final String userId
    ) {
        sessionService.validate(sessionDto, Role.ADMIN);
        return sessionService.findAllByUserId(userId);
    }

    @Override
    @WebMethod
    public Result closeAllSessionsByUserId(@WebParam(name = "session") @NotNull final SessionDto sessionDto,
                                           @WebParam(name = "userId") @Nullable final String userId
    ) {
        sessionService.validate(sessionDto, Role.ADMIN);
        try {
            sessionService.closeAllByUserId(userId);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
