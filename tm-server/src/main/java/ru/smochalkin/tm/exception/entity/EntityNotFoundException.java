package ru.smochalkin.tm.exception.entity;

import ru.smochalkin.tm.exception.AbstractException;

public final class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error! Entity not found...");
    }

}
