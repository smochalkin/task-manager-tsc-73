package ru.smochalkin.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.dto.ProjectDto;
import ru.smochalkin.tm.enumerated.Sort;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.repository.dto.ProjectRepository;

import java.util.*;
import java.util.stream.Collectors;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

@Service
public class ProjectDtoService extends AbstractDtoService<ProjectDto> implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final ProjectDto project = new ProjectDto();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<ProjectDto> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDto findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        projectRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public int getCount() {
        return (int) projectRepository.count();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        projectRepository.deleteByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<ProjectDto> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<ProjectDto> findAll(@NotNull final String userId, @NotNull final String sort) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<ProjectDto> comparator = sortType.getComparator();
        return projectRepository.findAllByUserId(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDto findById(@NotNull final String userId, @Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return projectRepository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDto findByName(@NotNull final String userId, @NotNull final String name) {
        return projectRepository.findFirstByUserIdAndName(userId, name);
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDto findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        List<ProjectDto> list = projectRepository.findAllByUserId(userId);
        if (list.isEmpty()) return null;
        return projectRepository.findAllByUserId(userId).get(index);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        projectRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final ProjectDto project = findByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();
        projectRepository.delete(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @Nullable final String desc
    ) {
        @Nullable final ProjectDto project = findById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        project.setName(name);
        project.setDescription(desc);
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @Nullable final String desc
    ) {

        @Nullable final ProjectDto entity = findByIndex(userId, index);
        if (entity == null) throw new EntityNotFoundException();
        entity.setName(name);
        entity.setDescription(desc);
        projectRepository.save(entity);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String strStatus
    ) {
        @Nullable final ProjectDto project = findById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        Status status = Status.getStatus(strStatus);
        Map<String, Date> dateMap = prepareDates(status);
        project.setStatus(status);
        project.setStartDate(dateMap.get("start_date"));
        project.setEndDate(dateMap.get("end_date"));
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String strStatus
    ) {
        @Nullable final ProjectDto project = findByName(userId, name);
        if (project == null) throw new EntityNotFoundException();
        Status status = Status.getStatus(strStatus);
        Map<String, Date> dateMap = prepareDates(status);
        project.setStatus(status);
        project.setStartDate(dateMap.get("start_date"));
        project.setEndDate(dateMap.get("end_date"));
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String strStatus
    ) {
        @Nullable final ProjectDto project = findByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();
        Status status = Status.getStatus(strStatus);
        Map<String, Date> dateMap = prepareDates(status);
        project.setStatus(status);
        project.setStartDate(dateMap.get("start_date"));
        project.setEndDate(dateMap.get("end_date"));
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    public boolean isNotIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) return true;
        return index >= projectRepository.countByUserId(userId);
    }

    @SneakyThrows
    private Map<String, Date> prepareDates(@NotNull final Status status) {
        Map<String, Date> map = new HashMap<>();
        switch (status) {
            case IN_PROGRESS:
                map.put("start_date", new Date());
                map.put("end_date", null);
                break;
            case COMPLETED:
                map.put("start_date", null);
                map.put("end_date", new Date());
                break;
            case NOT_STARTED:
                map.put("start_date", null);
                map.put("end_date", null);
        }
        return map;
    }

}
