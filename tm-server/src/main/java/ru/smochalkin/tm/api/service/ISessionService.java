package ru.smochalkin.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.dto.SessionDto;

import java.util.List;

public interface ISessionService extends IService<SessionDto> {

    SessionDto open(@NotNull String login, @NotNull String password);

    void close(@NotNull SessionDto sessionDto);

    void closeAllByUserId(@Nullable String userId);

    void validate(@NotNull SessionDto sessionDto);

    @SneakyThrows
    void validate(@Nullable SessionDto sessionDto, @Nullable Role role);

    @NotNull List<SessionDto> findAllByUserId(@Nullable String userId);

}
